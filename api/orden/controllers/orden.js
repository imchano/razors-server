'use strict';
const stripe = require('stripe')('sk_test_51H0BDiBF0cbCaEgAgo2vKQr2POONanJZdKwq5Pu04aaJ2j24m7dJCVHIesp9clEJP2TRxSALc0KRYovHOuPk7n3000VL0mZKml');

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

module.exports = {
    create: async context => {
        const {nombre, total, items, tokenStripe} = context.request.body;
        const {id} = context.state.user;

        const cargo = await stripe.charges.create({
            amount: total * 100,
            currency: 'usd',
            description: `Orden de ${context.state.user.username} del ${new Date()}`,
            source: tokenStripe
        });

        const orden = await strapi.services.orden.create({nombre, total, items, user: id});
        return orden;
    }
};
